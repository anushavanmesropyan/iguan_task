$('.change-password').on('click', function (e) {
    e.preventDefault();
    $($(this).attr('href')).modal('show');
    $('#url').val($(this).attr('data-url'));
    $('#item-id-val').val($(this).attr('data-id'));
});
$('.save-password').on('click', function (e) {
    e.preventDefault();
    $('.save-password').prop("disabled", true);
    var password = $('#changePasswordVal').val();
    var id = $('#item-id-val').val();
    var url = $('#url').val();
    var modalId = '#changePassword';
    if(id && url){
        console.log(url)
        console.log(id)
        $.ajax({
            url: url,
            type: 'POST',
            data: {id, password},
            success: function (data) {
                if (data) {
                    if (data.success) {
                        $(modalId + ' .modal-body').prepend(`<div id="successfully">${data.message}</div>`);
                        $(modalId + ' .form-group').hide();
                        setTimeout(() => {
                            $(modalId).modal('hide');
                            $('.save-password').prop("disabled", false);
                            $('#successfully').remove();
                            $('#changePasswordVal').val('');
                            $(modalId + ' .form-group').show();
                        }, 1600);
                    }
                }
            },
            error: function (e) {
                console.log(e)
            }
        })
    }
});