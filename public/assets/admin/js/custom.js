/**
 * Token App
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
/**
 * Data table
 */
$('#dataTable').DataTable(
    {
        "aoColumnDefs": [
            {'bSortable': false, 'aTargets': [4]},
            {'bSortable': false, 'aTargets': [5]}
        ]
    }
);
/**
 * Delete Item
 */
$('body').on('click', '.delete-item', function (e) {
    e.preventDefault();
    $('#url').val($(this).attr('data-url'));
    $('#item-id-val').val($(this).attr('data-id'));
});
$('body').on('click', '.delete', function (e) {
    e.preventDefault();
    var id = $('#item-id-val').val();
    var url = $('#url').val();
    var this_ = $(this);
    this_.prop("disabled", true);
    $.ajax({
        url: url,
        type: 'DELETE',
        success: function (data) {
            if (data) {
                if (data.success) {
                    this_.prop("disabled", false);
                    $('#deleteModal').modal('hide');
                    $('#delete-item-' + data.id).remove();
                }
            }
        },
        error: function (e) {
            console.log(e)
        }
    })
})