$(document).ready(function () {
    /**
     * Token App
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    /**
     * Delete Post
     */
    $('body').on('click', '.delete-post', function (e) {
        e.preventDefault();
        $('#item-id-val').val($(this).attr('data-id'));
        $('#url').val($(this).attr('data-url'));
    });
    $('body').on('click', '.delete', function (e) {
        e.preventDefault();
        let id = $('#item-id-val').val();
        let url = $('#url').val();
        let this_ = $(this);
        this_.prop("disabled", true);
        $.ajax({
            url: url,
            type: 'DELETE',
            success: function (data) {
                if (data) {
                    if (data.success) {
                        $('#deleteModal').modal('hide');
                        this_.prop("disabled", false);

                        $('#post-item-' + data.id).remove();
                    }
                }
            },
            error: function (e) {
                console.log(e)
            }
        })
    })
    /**
     * Like Post
     */
    $('body').on('click', '.like-post', function (e) {
        e.preventDefault();
        if ($('#guest_custom').val()) {
            $('#regLoginModal').modal({
                'show': true,

            });
        } else {
            let url = $(this).attr('data-url');
            let id = $(this).attr('data-post-id');
            let value = $(this).attr('data-value');
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    id,
                    value,
                },
                success: function (data) {
                    if (data) {
                        if (data.success) {
                            //  $('#post-item-'+data.id).remove();
                            $('#like_' + id).html(data.like_items)
                        }
                    }
                },
                error: function (e) {
                    console.log(e)
                }
            })
        }

    })
});