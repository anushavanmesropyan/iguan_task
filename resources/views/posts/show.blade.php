@extends('layouts.master')
@section('content')

    <div class="card">
        <div class="card-header">
            Post #{{$post['id']}}
            <a class="btn btn-success float-right" href="{{route('posts.create')}}" role="button">Add post</a>


        </div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="album ">
                <div class="container">
                    <div class="row">
                            <div class="col-md-12">
                                <h1>{{$post['title']}}</h1>
                                <p>
                                    {!! $post['description']!!}
                                </p>
                                <div>
                                    <span class="badge">Posted {{$post['created_at']}}</span>
                                </div>
                                <hr>
                            </div>


                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection