@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header clearfix">
            Post Update
            <a href="{{route('home')}}" class="btn btn-primary  float-right ">Go home</a>
        </div>

        <div class="card-body ">
            @include('posts.form',array('method'=>'POST','update'=>true,'action'=> route('posts.update',array('post'=>$post['id']))))
        </div>
    </div>
    @if(isset($errors))
    @endif
@endsection