@extends('layouts.master')
@section('content')

                <div class="card">
                    <div class="card-header clearfix">
                        Post create
                        <a href="{{route('home')}}" class="btn btn-primary  float-right ">Go home</a>
                    </div>

                    <div class="card-body ">
                        @include('posts.form',array('method'=>'POST','action'=> route('posts.store')))
                    </div>
                </div>
    @if(isset($errors))
    @endif
@endsection