<form action="{{$action}}" method="{{$method}}">
    @csrf
    @if(isset($update))
        @method('PUT')
    @endif
    <div class="form-group">
        <label for="title">Title</label>
        <input
                type="text"
                name="title"
                class="form-control title-post {{$errors->has('title')?'text-danger is-invalid':(old('title')?'is-valid':'')}}"
                id="title"
                value="{{isset($post)?old('title',$post['title']):old('title') }}"
                placeholder="Title">
        @if($errors->has('title'))
            <small class="text-danger error">
                {{ $errors->first('title') }}
            </small>
        @endif

    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea
                class="form-control description-post  {{$errors->has('description')?'text-danger is-invalid':(old('description')?'is-valid':'')}}"
                name="description"
                id="description"
                rows="3">{{isset($post)?old('description',$post['description']):old('description') }}
        </textarea>
        @if($errors->has('description'))
            <small class="text-danger error">
                {{ $errors->first('description') }}
            </small>
        @endif
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>