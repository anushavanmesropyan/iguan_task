<form action="{{$action}}" method="{{$method}}">
    @csrf
    @if(isset($update))
        @method('PUT')
    @endif
    <div class="form-group">
        <label for="title">Name</label>
        <input
                type="text"
                name="name"
                class="form-control title-post {{$errors->has('name')?'text-danger is-invalid':(old('name')?'is-valid':'')}}"
                id="name"
                value="{{isset($user)?old('name',$user['name']):old('name') }}"
                placeholder="Name">
        @if($errors->has('name'))
            <small class="text-danger error">
                {{ $errors->first('name') }}
            </small>
        @endif
    </div>
    <div class="form-group">
        <label for="title">Email</label>
        <input
                type="email"
                name="email"
                class="form-control title-post {{$errors->has('email')?'text-danger is-invalid':(old('email')?'is-valid':'')}}"
                id="email"
                value="{{isset($user)?old('email',$user['email']):old('email') }}"
                placeholder="Email">
        @if($errors->has('email'))
            <small class="text-danger error">
                {{ $errors->first('email') }}
            </small>
        @endif
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>