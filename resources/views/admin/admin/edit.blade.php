@extends('admin.layouts.master')
@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Admin Edit</h1>
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <div class="container">
                    <div class="col-md-8 offset-md-2">
                        @include('admin.admin.form',array('method'=>'POST','update'=>true,'action'=> route('admin.users.update',array('user'=>$user['id']))))
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection