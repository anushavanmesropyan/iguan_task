@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center">
                        Change Admin Password
                    </div>
                    <div class="card-body">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('flash_message') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('admin.changePassword.storeAdminPassword') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Current
                                    Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="current_password"
                                           autocomplete="current-password">
                                    @if($errors->has('current_password'))
                                        <small class="text-danger error">
                                            {{ $errors->first('current_password') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>
                                <div class="col-md-6">
                                    <input id="new_password" type="password" class="form-control" name="new_password"
                                           autocomplete="current-password">
                                    @if($errors->has('new_password'))
                                        <small class="text-danger error">
                                            {{ $errors->first('new_password') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm
                                    Password</label>

                                <div class="col-md-6">
                                    <input id="new_confirm_password" type="password" class="form-control"
                                           name="new_confirm_password" autocomplete="current-password">
                                    @if($errors->has('new_confirm_password'))
                                        <small class="text-danger error">
                                            {{ $errors->first('new_confirm_password') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection