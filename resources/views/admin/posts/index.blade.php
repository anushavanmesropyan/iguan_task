@extends('admin.layouts.master')
@section('content')


    <!-- Page Heading -->
    <div class="page-heading clearfix mb-2">
        <h1 class="h3 mb-2 text-gray-800 fa-pull-left">Posts</h1>
        <a href="{{route('admin.posts.create')}}" class="btn btn-success fa-pull-right">Create</a>
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>User name</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>User name</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($posts as $post)
                        <tr id="delete-item-{{$post['id']}}">
                            <td>{{$post['id']}}</td>
                            <td>{{$post->user->name}}</td>
                            <td>{{$post['title']}}</td>
                            <td style="max-width:600px">{{$post['description']}}</td>
                            <td>
                                <a href="{{route('admin.posts.edit',array('post'=>$post['id']))}}" class="btn btn-warning btn-icon-split dtc_edit">
                                    <span class="text">Edit</span>
                                </a>
                            </td>
                            <td>
                                <a
                                        href="#deleteModal"
                                        data-toggle="modal"
                                        class="btn btn-danger btn-icon-split delete-item"
                                        data-url="{{route('admin.posts.destroy',array('post'=>$post['id']))}}"
                                        data-id="{{$post['id']}}">
                                    <span class="text">Delete</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('modals.delete')
    <input type="hidden" id="url" value="">
    <input type="hidden" id="item-id-val" value="">
@endsection
@section('script')
    <script src="{{asset('admin/js/admin-post.js')}}"></script>
@endsection