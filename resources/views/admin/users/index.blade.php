@extends('admin.layouts.master')
@section('content')


    <!-- Page Heading -->
    <div class="page-heading clearfix mb-2">
        <h1 class="h3 mb-2 text-gray-800 fa-pull-left">Users</h1>
        <a href="{{route('admin.users.create')}}" class="btn btn-success fa-pull-right">Create</a>
    </div>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Email</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Change Password</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Email</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Change Password</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($users as $user)
                        <tr id="delete-item-{{$user['id']}}">
                            <td>{{$user['id']}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->role->role}}</td>
                            <td style="max-width:600px">{{$user['email']}}</td>
                            <td >
                                <a href="{{route('admin.users.edit',array('user'=>$user['id']))}}" class="btn btn-warning btn-icon-split">
                                    <span class="text">Edit</span>
                                </a>
                            </td>
                            <td>
                                <a
                                        href="#deleteModal"
                                        data-toggle="modal"
                                        data-url="{{route('admin.users.destroy',array('user'=>$user['id']))}}"
                                        data-id="{{$user['id']}}"
                                        class="btn btn-danger btn-icon-split delete-item">
                                    <span class="text">Delete</span>
                                </a>
                            </td>
                            <td>
                                <a
                                        href="#changePassword"
                                        data-url="{{route('admin.changePassword.changeUserPassword')}}"
                                        data-id="{{$user['id']}}"
                                        class="btn btn-dark btn-icon-split change-password">
                                    <span class="text">Change</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('modals.delete')
    @include('modals.changePassword')
    <input type="hidden" id="url" value="">
    <input type="hidden" id="item-id-val" value="">
@endsection
@section('script')
    <script src="{{asset('admin/js/user-index.js')}}"></script>
@endsection