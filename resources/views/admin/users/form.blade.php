<form action="{{$action}}" method="{{$method}}">
    @csrf
    @if(isset($update))
        @method('PUT')
    @endif
    <div class="form-group">
        <label for="title">Name</label>
        <input
                type="text"
                name="name"
                class="form-control title-post {{$errors->has('name')?'text-danger is-invalid':(old('name')?'is-valid':'')}}"
                id="name"
                value="{{isset($user)?old('name',$user['name']):old('name') }}"
                placeholder="Name">
        @if($errors->has('name'))
            <small class="text-danger error">
                {{ $errors->first('name') }}
            </small>
        @endif
    </div>
    <div class="form-group">
        <label for="title">Email</label>
        <input
                type="email"
                name="email"
                class="form-control title-post {{$errors->has('email')?'text-danger is-invalid':(old('email')?'is-valid':'')}}"
                id="email"
                value="{{isset($user)?old('email',$user['email']):old('email') }}"
                placeholder="Email">
        @if($errors->has('email'))
            <small class="text-danger error">
                {{ $errors->first('email') }}
            </small>
        @endif
    </div>
    <div class="form-group">
        <label for="role">Role</label>
        <select class="form-control form-control-lg {{$errors->has('role_id')?'text-danger is-invalid':(old('role_id')?'is-valid':'')}}"
                id="role" name="role_id">
            @if(!isset($user))
                <option value="">Select role user</option>
            @endif
            @foreach($roles as $role)
                <option value="{{$role['id']}}" {{ isset($user) && $role['id'] === $user['role_id'] ? 'selected':'' }}>{{$role['role']}}</option>
            @endforeach
        </select>
        @if($errors->has('role'))
            <small class="text-danger error">
                {{ $errors->first('role') }}
            </small>
        @endif
    </div>
    @if(!isset($update))
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
            @if($errors->has('password'))
                <small class="text-danger error">
                    {{ $errors->first('password') }}
                </small>
            @endif
        </div>
    @endif

    <div class="form-group text-center">
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>