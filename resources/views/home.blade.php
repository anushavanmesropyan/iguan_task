@extends('layouts.master')
@section('seo')
    <title>{{ config('app.name', 'Iguan Task') }}</title>
    <meta name="keywords" content="Iguan Task">
    <meta name="description" content="Iguan Task">
    <meta property="og:description" content="Iguan Task">
    <meta property="og:url" content="{{url()->current()}}">
    <meta property="og:title" content="Iguan Task">
@endsection
@section('content')


    <div class="card">
        <div class="card-header">
            Posts
            <a class="btn btn-success float-right" href="{{route('posts.create')}}" role="button">Add post</a>
        </div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="album ">
                <div class="container">
                    <div class="row">
                        @foreach($posts as $k=>$post)
                            <div class="col-md-6" id="post-item-{{$post['id']}}">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <p class="card-text clearfix">
                                            <a
                                                    href="{{route('posts.show',['id'=>$post['id'],'slug'=>$post['slug']])}}"
                                                    class="text-dark text-decoration-none float-left ">
                                                {{$post['title']}}
                                            </a>
                                        {{--<button
                                                data-url="{{route('posts.like',array('post'=>$post['id']))}}"
                                                class="btn   btn-sm float-right text-primary like-post">
                                            <i class="fa fa-thumbs-up"></i>
                                        </button>--}}
                                        <div class="like_block" id="like_{{$post['id']}}">
                                            @include('likes.like-items',array(
                                            'url'=>route('like.post'),
                                            'id'=>$post['id'],
                                            'likesCnt'=>$post->likes()->count(),
                                            'dislikesCnt'=>$post->dislikes()->count(),
                                            ))
                                        </div>
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group" role="group">
                                                <a href="{{route('posts.show',['id'=>$post['id'],'slug'=>$post['slug']])}}"
                                                   class="btn btn-sm btn-outline-secondary">
                                                    View
                                                </a>
                                                @if(auth()->check() && auth()->user()->isAdmin())
                                                    <a
                                                            href="{{route('admin.posts.edit',array('post'=>$post['id']))}}"
                                                            class="btn btn-sm btn-outline-secondary">
                                                        Edit
                                                    </a>
                                                    <a
                                                            href="#deleteModal"
                                                            data-toggle="modal"
                                                            data-id="{{$post['id']}}"
                                                            data-url="{{route('admin.posts.destroy',array('post'=>$post['id']))}}"
                                                            class="btn btn-sm btn-outline-secondary delete-post">
                                                        Delete
                                                    </a>
                                                @endif
                                                @if($post['user_id']===auth()->id() && !auth()->user()->isAdmin())
                                                    <a
                                                            href="{{route('posts.edit',array('post'=>$post['id']))}}"
                                                            class="btn btn-sm btn-outline-secondary">
                                                        Edit
                                                    </a>
                                                    <a
                                                            href="#deleteModal"
                                                            data-toggle="modal"
                                                            data-id="{{$post['id']}}"
                                                            data-url="{{route('posts.destroy',array('post'=>$post['id']))}}"
                                                            class="btn btn-sm btn-outline-secondary delete-post">
                                                        Delete
                                                    </a>
                                                @endif
                                            </div>


                                            <small class="text-muted">{{date('d-m-Y', strtotime($post['created_at']))}}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="col-md-12 pagination-container text-center">
                            {{ $posts->links() }}
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <input class="hidden" type="hidden" value="1" name="guest_custom" id="url">
    <input class="hidden" type="hidden" value="1" name="guest_custom" id="item-id-val">

    @include('modals.delete')
    @include('modals.login_registration',array('message'=>'For like and dislike, you need to register on the site.Do you have a profile?'))
    @if (Auth::guest())
        <input class="hidden" type="hidden" value="1" name="guest_custom" id="guest_custom">
    @endif
@endsection
@section('script')
    <script src="{{asset('js/home.js')}}"></script>
@endsection