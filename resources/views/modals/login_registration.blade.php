<div class="modal" tabindex="-1" role="dialog" id="regLoginModal">
    <div class="modal-dialog modal-sm"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    {{$message}}
                </p>
                <div class="float-right">
                    <a role="button" href="{{ route('login') }}" class="btn btn-secondary">{{ __('Login') }}</a>
                    <a role="button" href="{{ route('register') }}" class="btn btn-dark">{{ __('Register') }}</a>
                </div>
            </div>

        </div>
    </div>
</div>