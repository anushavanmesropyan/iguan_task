<button
        data-url="{{$url}}"
        data-post-id="{{$id}}"
        data-value="1"
        class="like like-post ">
    <i class="fa fa-thumbs-up">
    </i>
    {{$likesCnt}}
</button>
<button
        data-url="{{$url}}"
        data-post-id="{{$id}}"
        data-value="0"
        class="dislike like-post ">
    <i class="fa fa-thumbs-down"></i>
    {{$dislikesCnt}}
</button>