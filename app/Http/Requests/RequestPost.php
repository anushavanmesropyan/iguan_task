<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class RequestPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:191',
            'description' => 'required',
            'slug' => 'required|max:191',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'A Title is required',
            'title.max:191' => 'A title max length 191',
            'description.required'  => 'A Description is required',
        ];
    }

    /**
     *
     */
    public function prepareForValidation(){
        $this->merge([
            'slug' => Str::slug($this->title, '-'),
            'user_id' => !$this->role_id ? auth()->id() : $this->role_id,
        ]);
    }

}
