<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Rules\MatchOldPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ChangePasswordController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeUserPassword(Request $request){
        if (request()->ajax()) {
            $user = User::find($request->id);
            if($user){
                $user->update([
                    'password'=>Hash::make($request->password),
                ]);
                return response()->json(['success' => true, 'message' => 'Change user password successfully']);
            }

            return response()->json(['success' => false]);
        }
        abort(404);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createAdminPassword()
    {
        return view('admin.admin.change-password-form');
    }

    /**
     * @param Request $request
     */
    public function storeAdminPassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
        Session::flash('flash_message', 'Password change successfully.');
        return redirect()->back();
    }
}
