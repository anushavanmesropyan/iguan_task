<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostLikeRequest;
use App\Models\Like;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class LikeController extends Controller
{
    /**
     * Like Post
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function postLike(PostLikeRequest $request)
    {
        if(request()->ajax())
        {
            $post = Post::with(['likes','dislikes'])->find($request->id);
            if($post){
                $like = Like::where('post_id', '=', $post->id)->where('user_id', '=', auth()->id())->first();
                if(!$like){
                    Like::create([
                        'user_id'=>auth()->id(),
                        'post_id'=>$post->id,
                        'value'=>$request->value
                    ]);
                }else{
                    if($request->value != $post->value){
                        Gate::authorize('update', $like);
                        $like->update([
                            'user_id'=>auth()->id(),
                            'post_id'=>$post->id,
                            'value'=>$request->value
                        ]);
                    }
                }
                $like_items = view('likes.like-items')->with([
                    'url'=>route('like.post'),
                    'id'=>$post['id'],
                    'likesCnt'=>$post->likes()->count(),
                    'dislikesCnt'=>$post->dislikes()->count(),
                ])->render();

                return response()->json(['success' => true,'like_items'=>$like_items]);
            }

            return response()->json(['success' => false]);
        }
        abort(404);
    }
}
