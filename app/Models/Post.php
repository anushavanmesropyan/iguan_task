<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * @var array
     */
    protected $fillable =
        [
            'user_id',
            'title',
            'description',
            'slug'
        ];

    /**
     * table likes
     * Get the likes for the blog post.
     */
    public function likes()
    {
        return $this->hasMany(Like::class)->where('value','=',1);
    }

    /**
     * table likes
     * Get the dislikes for the blog post.
     */
    public function dislikes()
    {
        return $this->hasMany(Like::class)->where('value','=',0);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
