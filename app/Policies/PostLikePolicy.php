<?php

namespace App\Policies;

use App\Models\Like;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PostLikePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * Determine if the given post can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Like  $like
     * @return bool
     */
    public function update(User $user, Like $like)
    {
        return $user->id === $like->user_id
            ? Response::allow()
            : Response::deny('You do not own this post.');
    }
}
