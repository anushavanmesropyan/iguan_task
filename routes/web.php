<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::resource('posts', 'Site\\PostController')->except('show')->middleware('auth');
Route::get('/posts/{id}/{slug}', 'Site\\PostController@show')->name('posts.show');
Route::post('/post/like', 'Site\\LikeController@postLike')->name('like.post')->middleware('auth');
/**
 * Admin
 */
Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware' => ['is_admin','auth'],'as'=>'admin.'], function () {
    Route::get('/', 'AdminController@index')->name('home');
    Route::resource('posts', 'PostController')->except('show')->middleware('auth');
    Route::resource('users', 'UserController')->except('show')->middleware('auth');
    Route::post('/changeUserPassword', 'ChangePasswordController@changeUserPassword')->name('changePassword.changeUserPassword')->middleware('auth');
    Route::get('/edit', 'AdminController@edit')->name('edit');
    Route::get('change-password', 'ChangePasswordController@createAdminPassword')->name('changePassword.createAdminPassword');
    Route::post('change-password', 'ChangePasswordController@storeAdminPassword')->name('changePassword.storeAdminPassword');
});
