<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'role_id'=> \App\Models\Role::where('role','=','user')->first()->id,
                'name' => 'User',
                'email' => 'user@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'role_id'=> \App\Models\Role::where('role','=','user')->first()->id,
                'name' => 'Example',
                'email' => 'example@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'role_id'=> \App\Models\Role::where('role','=','user')->first()->id,
                'name' => 'Test',
                'email' => 'test@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'role_id'=> \App\Models\Role::where('role','=','user')->first()->id,
                'name' => 'User 2',
                'email' => 'anuashavan@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'role_id'=> \App\Models\Role::where('role','=','admin')->first()->id,
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'role_id'=> \App\Models\Role::where('role','=','admin')->first()->id,
                'name' => 'Admin 2',
                'email' => 'admin2@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'role_id'=> \App\Models\Role::where('role','=','user')->first()->id,
                'name' => Str::random(10),
                'email' => Str::random(10) . '@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'role_id'=> \App\Models\Role::where('role','=','user')->first()->id,
                'name' => Str::random(10),
                'email' => Str::random(10) . '@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'role_id'=> \App\Models\Role::where('role','=','user')->first()->id,
                'name' => Str::random(10),
                'email' => Str::random(10) . '@gmail.com',
                'password' => Hash::make('12345678'),
            ],
        ]);
    }
}
